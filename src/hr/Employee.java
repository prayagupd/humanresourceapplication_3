package hr;

public class Employee {

	private int employeedId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String birthDate;
	private String SSN;
	private double salary;

	public Employee(int employeedId, String firstName, String middleName,
			String lastName, String birthDate, String sSN, double salary) {
		super();
		this.employeedId = employeedId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		SSN = sSN;
		this.salary = salary;
	}

	public String print() {

		StringBuilder builder = new StringBuilder();
		builder.append("Employeed First Name : " + firstName);
		builder.append("Employeed Middle  Name : " + middleName);
		builder.append("Employeed Last  Name : " + lastName);
		builder.append("Employeed Birth date : " + birthDate);
		builder.append("Employeed SSN : " + SSN);
		builder.append("Employeed Salary : " + salary);
		builder.append("\n");
		System.out.println(builder.toString());
		return builder.toString();
	}

	public int getEmployeedId() {
		return employeedId;
	}

	public void setEmployeedId(int employeedId) {
		this.employeedId = employeedId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getSSN() {
		return SSN;
	}

	public void setSSN(String sSN) {
		SSN = sSN;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

}
