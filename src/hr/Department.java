package hr;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Department {

	private String name;
	private String location;
	private ArrayList<Position> posList;

	public Department(String name, String location) {
		super();
		this.name = name;
		this.location = location;
	}

	public String print() {

		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("------Department----------");
		stringBuilder.append("\n");
		stringBuilder.append("Department Name : " + name);
		stringBuilder.append("\n");
		stringBuilder.append("Department Location : " + location);
		System.out.println(stringBuilder.toString());
		stringBuilder.append("Positions: ");
		for (Iterator<Position> iterator = posList.iterator(); iterator
				.hasNext();) {
			Position pos = iterator.next();
			pos.print();
			//System.out.println(pos.print());
		}
		stringBuilder.append("\n");

		//System.out.println(stringBuilder.toString());
		return stringBuilder.toString();

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void addPosition(Position pos1) {
		if (posList == null)
			posList = new ArrayList<Position>();

		posList.add(pos1);

	}

	public double getSalary() {
	
		double salary=0.0;
		for (Iterator<Position> iterator = posList.iterator(); iterator.hasNext();) {
			Position pos= iterator.next();
			if(pos.getEmployee()!=null)
			salary+= pos.getEmployee().getSalary();
		}
		return salary;
	}

	public void printReportingHierarchy(){
		Position deptHead = this.getDepartmentHead();
		if(deptHead == null){
			System.out.println("No Department Head Found");
			return;
		}
		deptHead.printDownLine();
	}
	
	public Position getDepartmentHead(){
		for(Position pos:posList){
			Position superior = pos.getReportsTo();
			if(superior == null){
				continue;
			}
			
			if(!pos.getReportsTo().getDepartment().equals(this)){
				return pos;
			}
		}
		return null;
	}
	
	public boolean equals(Department dept){
		if(this.name.equals(dept.getName()) && this.location.equals(dept.getLocation())){
			return true;
		}
		return false;
	}
}
