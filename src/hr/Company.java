package hr;

import java.util.ArrayList;
import java.util.Iterator;

public class Company {

	private String name;
	private ArrayList<Department> depList;

	public Company(String name, ArrayList<Department> depList) {
		super();
		this.name = name;
		this.depList = depList;
	}

	public Company(String name) {
		super();
		this.name = name;
	}

	public void addDepartment(Department dep) {
		if (depList == null)
			depList = new ArrayList<Department>();
		depList.add(dep);
	}
	
	
	public double getSalary()
	{
		double salary=0.0;
		for (Iterator<Department> iterator = depList.iterator(); iterator.hasNext();) {
			Department  depart = iterator.next();
			salary+=depart.getSalary();
		}
		return salary;	
	}

	public String print() {

		StringBuilder builder = new StringBuilder();
		builder.append("-------Company----------");
		builder.append("\n");
		builder.append("Company Name " + name);

		System.out.println(builder.toString());
		for (Iterator<Department> iterator = depList.iterator(); iterator
				.hasNext();) {
			Department dep = iterator.next();
			dep.print();
		}

		builder.append("\n");
		
		return builder.toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Department> getDepList() {
		return depList;
	}

	public void setDepList(ArrayList<Department> depList) {
		this.depList = depList;
	}

}