package hr.composition;

import hr.Company;
import hr.Department;
import hr.Employee;
import hr.Position;

public class Application {


	public static void main(String args[]) {

		// New Company
		Company compny = new Company("Spotify");

		// New Department
		Department engineering = new Department("Engineering", "San Francisco");
		Department hr = new Department("HR", "Redmond");

		// Adding department to company
		compny.addDepartment(engineering);
		compny.addDepartment(hr);

		// New Position
		Position pos5 = new Position("Senior Software Engineer", "Senior Level", engineering);
		
		Position pos1 = new Position("HR Cordinator", "Cordinates all teams", pos5, engineering);
		Position pos11 = new Position("Vice HR Cordinator", "Cordinates all teams", pos1, hr);
		pos1.addInferior(pos11);
		
		Position pos2 = new Position("Recruiter",
				"Responsible for all recruitement", pos1, hr);
		Position pos3 = new Position("Hr Manager",
				"Responsible for all department management", pos1, hr);

		Position pos4 = new Position("Software Engineer", "Entry Level", engineering);
		
		Position pos6 = new Position("Network Engineer",
				"Network Engineer Entry Level", engineering);

		// Adding positions to Department
		hr.addPosition(pos1);
		hr.addPosition(pos2);
		hr.addPosition(pos3);

		engineering.addPosition(pos4);
		engineering.addPosition(pos5);
		engineering.addPosition(pos6);

		// Adding Employee
		Employee employee1 = new Employee(1, "John", "Steve", "Jobs",
				"11-feb-1972", "12-3-123-333", 2000);
		Employee employee2 = new Employee(1, "Rashed", "Ibrahim",
				"El Ghanooshy", "15-mar-1962", "12-3-2222-333", 4000);

		// Filing position
		pos1.fillPosition(employee1);

		pos6.fillPosition(employee2);

	
	//Printing Company
		compny.print();
	
		System.out.println("-----------------The Company All salary-------------");
		System.out.println("The Company All salary "+compny.getSalary());
		
		System.out.println("-----------------Position printDownLine-------------");
		System.out.println("-----------------------------------------------------");
		pos1.printDownLine();

		hr.printReportingHierarchy();
	}
}
