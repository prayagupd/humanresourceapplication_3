package hr;

import java.util.ArrayList;
import java.util.List;

public class Position {

	private String title;
	private String description;
	private Employee employee;
	private Position reportsTo;
	private Department department;
	private List<Position> inferiors = new ArrayList<Position>();
	private boolean manager;
	

	public Position(String title, String description, Employee employee, Position reportsTo) {
		super();
		this.title = title;
		this.description = description;
		this.employee = employee;
		this.reportsTo = reportsTo;
	}

	
	public Position(String title, String description, Employee employee) {
		super();
		this.title = title;
		this.description = description;
		this.employee = employee;
	}

	public Position(String title, String description, Position reportsTo, Department department) {
		super();
		this.title = title;
		this.description = description;
		this.reportsTo = reportsTo;
		this.department = department;
	}
	
	public Position(String title, String description, Department department) {
		super();
		this.title = title;
		this.description = description;
		this.department = department;
	}

	public String print() {

		StringBuilder builder = new StringBuilder();
		builder.append("Position Tiltle :");
		builder.append(title);

		builder.append("Position description : ");
		builder.append(description);
		System.out.println(builder.toString());
		builder.append("\n");
		builder.append("Employee Details for filled position if exist :");
		builder.append("\n");
		if (employee != null)
			builder.append(employee.print());
		else
			builder.append("Not filled position");
		builder.append("\n");
		
		
		return builder.toString();

	}

	public void fillPosition(Employee employee) {

		this.employee = employee;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Position getReportsTo() {
		return reportsTo;
	}

	public void setReportsTo(Position reportTo) {
		this.reportsTo = reportTo;
	}
	
	public boolean isManager() {
		return manager;
	}


	public void setManager(boolean manager) {
		this.manager = manager;
	}
	
	public void addInferior(Position inferior) {
		this.inferiors.add(inferior);
	}
	
	public void printDownLine(){
		StringBuilder builder = new StringBuilder();
		builder.append("Position Title :");
		builder.append(title);
		
		System.out.println(builder);
		for (Position inferior : this.inferiors) {
			inferior.printDownLine();
		}
	}


	public Department getDepartment() {
		return department;
	}


	public void setDepartment(Department department) {
		this.department = department;
	}


	public List<Position> getInferiors() {
		return inferiors;
	}


	public void setInferiors(List<Position> inferiors) {
		this.inferiors = inferiors;
	}

	
}